const board = document.querySelector('#board');
const SQUARES_NUMBER = 800;
// const colors = ['#e74c3c', '#8e44ad', '#3498db', '#e67e22', '#2ecc71'];
// const colors = ['#E070AC', '#1F4EE0', '#E6E622', '#6BE644', '#E02A12', '#7F6EE0', '#E05D2D', '#E027CD'];
const colors = ['#CFBDD2', '#AC16C9', '#D5007F', '#55335C', '#C308C9', '#9C3398', '#D5BFD4', '#CC1FA1', '#CC60A1', '#D50060', '#BD62AC', '#F0C7E8'];

for(let i = 0; i < SQUARES_NUMBER; i++){
    const square = document.createElement('div');    //create 'div'
    square.classList.add('square');    //assign relevant CSS class

    square.addEventListener('mouseover', ()=>{
        setColor(square)
    })

    square.addEventListener('mouseleave', ()=>{
        removeColor(square)
    })

    board.append(square); //add 'square' to HTML
}

function setColor(element){
    const color = getRandomColor();
    element.style.backgroundColor = color;
    element.style.boxShadow = `0 0 2px ${color}, 0 0 10px ${color}`
}

function removeColor(element){
    element.style.backgroundColor = '#1d1d1d';
    element.style.boxShadow = '0 0 2px #000';
}

function getRandomColor(){
    const index = Math.floor(Math.random() * colors.length);
    return colors[index];
}
